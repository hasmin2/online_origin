/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.origin;

import com.streamsets.pipeline.api.BatchMaker;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.base.BaseSource;
import com.streamsets.stage.origin.connector.GeneralPortOpenCmd;
import com.streamsets.stage.origin.connector.HttpResponseCmd;
import com.streamsets.stage.origin.connector.PingCmd;

import java.text.MessageFormat;
import java.util.*;

import static java.lang.Thread.sleep;

/**
 * This source is an example and does not actually read from anywhere.
 * It does however, generate generate a simple record with one field.
 */
public abstract class OnlineSource extends BaseSource {
    private List <String> ipv4List;
    /**
     * Gives access to the UI configuration of the stage provided by the {@link OnlineDSource} class.
     */
    @Override
    protected List<ConfigIssue> init() {
        // Validate configuration values and open any required resources.
        List<ConfigIssue> issues = super.init();
        // If issues is not empty, the UI will inform the user of each configuration issue in the list.
        ipv4List = getIPMap();

        return issues;
    }

    /** {@inheritDoc} */
    @Override
    public void destroy() {
        // Clean up any open resources.
        super.destroy();
    }

    /** {@inheritDoc} */
    @Override
    public String produce(String lastSourceOffset, int maxBatchSize, BatchMaker batchMaker) {
        long nextSourceOffset = 0;
        if (lastSourceOffset != null) { nextSourceOffset = Long.parseLong(lastSourceOffset); }
        try {
            long startTime = System.currentTimeMillis();
            for (String item : ipv4List) {
                Map<String, Field> map = new HashMap<>();
                Record record = getContext().createRecord(String.valueOf(UUID.randomUUID()));
                if (usePing()) {
                    String result = new PingCmd().runPingCommand(item, getPingTimeout());
                    if (result.equals("")) { map.put("pingResult", Field.create(item));}
                    else { map.put("pingResult", Field.create(result)); }
                }
                if (usePortOpenStatus()){
                    GeneralPortOpenCmd generalPortOpenCmd = new GeneralPortOpenCmd();
                    generalPortOpenCmd.runNcCommand(item, getIpPort(), getPingTimeout());
                    map.put("portOpenResult", Field.create(MessageFormat.format("{0},{1},{2}", item, generalPortOpenCmd.getIsOnline(), generalPortOpenCmd.getResponseTime())));
                }
                if (useHttpresponse()) {
                    HttpResponseCmd response = new HttpResponseCmd();
                    int result = response.runHttpResponseCommand(item, getHttpPort(), getHttpSubAddress());
                    long responseTimegap = response.getTimegapLong();
                    map.put("httpResult", Field.create(MessageFormat.format("{0},{1},{2}", item, result, responseTimegap)));
                }
                record.set(Field.create(map));
                batchMaker.addRecord(record);
                nextSourceOffset++;
            }
            long endTime = System.currentTimeMillis();
            long interval = getInterval(startTime, endTime);
            sleep(interval);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        return String.valueOf(nextSourceOffset);
    }
    private long getInterval (long startTime, long endTime){
        long elaspedTime = (endTime - startTime);
        long interval = getPingInterval()*1000 - elaspedTime;
        return interval > 0 ? interval : 0;
    }

    public abstract List<String> getIPMap();
    public abstract boolean usePing();
    public abstract boolean usePortOpenStatus();

    public abstract boolean useHttpresponse();
    public abstract int getPingTimeout();
    public abstract int getPingInterval();

    public abstract String getHttpSubAddress();

    public abstract int getHttpPort();

    public abstract int getIpPort();
}

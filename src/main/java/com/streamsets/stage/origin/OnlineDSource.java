/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.streamsets.stage.origin;
import com.streamsets.pipeline.api.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@StageDef(
        version = 1,
        label = "Online Origin",
        description = "Checks the Host or Subnet has been online",
        icon = "online_origin.png",
        execution = ExecutionMode.STANDALONE,
        recordsByRef = true,
        onlineHelpRefUrl = ""
)
@ConfigGroups(value = Groups.class)
@GenerateResourceBundle
public class OnlineDSource extends OnlineSource {

    @ConfigDef(
            required = true,
            type = ConfigDef.Type.LIST,
            defaultValue = "",
            label = "IP Address",
            displayPosition = 10,
            group = "Network",
            description = "Input IP address for checking online status"
    )
    public List<String> ipAddressList = new ArrayList<>();
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = "true",
            label = "Ping",
            displayPosition = 10,
            group = "Network",
            description = "Online check performed by Ping"
    )
    public boolean isPing;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = "false",
            label = "General Port Status",
            displayPosition = 10,
            group = "Network",
            description = "Online check performed by nc -z -v"
    )
    public boolean isPortOpenStatus;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.BOOLEAN,
            defaultValue = "false",
            label = "Http Response",
            displayPosition = 10,
            group = "Network",
            description = "Online check performed by Http response"
    )
    public boolean isHttp;

    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "150",
            label = "Timeout for each Host (MilliSec)",
            displayPosition = 10,
            group = "Network",
            description = "Max timeout for each Host. Smaller timeout setting may ignore response from host. Bigger timeout setting may cause not to reach some hosts"
    )
    public int pingTimeout;

    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "150",
            label = "Each job batch interval (Sec)",
            displayPosition = 10,
            group = "Network",
            description = "Max Interval secs for pinging batch job. Smaller setting may ignore response from host. Bigger Setting may not recognize correct status "
    )
    public int pingInterval;

    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "80",
            label = "Port",
            displayPosition = 10,
            group = "Http",
            description = "Http Response Port to communicate",
            dependsOn = "isHttp",
            triggeredByValue = "true"
    )
    public int httpPort;

    @ConfigDef(
            required = false,
            type = ConfigDef.Type.STRING,
            defaultValue = "",
            label = "Sub Address",
            displayPosition = 10,
            group = "Http",
            description = "Sub Address ? for the Http Response proper path",
            dependsOn = "isPortOpenStatus",
            triggeredByValue = "true"
    )
    public String httpSubAddress;
    @ConfigDef(
            required = true,
            type = ConfigDef.Type.NUMBER,
            defaultValue = "80",
            label = "Port",
            displayPosition = 10,
            group = "GeneralPort",
            description = "Http Response Port to communicate",
            dependsOn = "isPortOpenStatus",
            triggeredByValue = "true"
    )
    public int ipPort;


    /** {@inheritDoc} */
    @Override
    public List<String> getIPMap() { return ipAddressList; }
    @Override
    public boolean usePing(){ return isPing; }
    @Override
    public boolean usePortOpenStatus(){ return isPortOpenStatus; }
    @Override
    public boolean useHttpresponse(){ return isHttp; }
    @Override
    public int getPingTimeout() { return pingTimeout; }
    @Override
    public int getPingInterval() { return pingInterval; }
    @Override
    public String getHttpSubAddress (){ return (null == httpSubAddress)? "":httpSubAddress; }
    @Override
    public int getHttpPort (){ return httpPort; }
    @Override
    public int getIpPort (){ return ipPort; }
}

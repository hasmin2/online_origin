package com.streamsets.stage.origin.connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class GeneralPortOpenCmd {
    private String result = null;
    private int isOnline;
    private int responseTime;
    public int getIsOnline() {return isOnline; }
    public int getResponseTime(){ return responseTime; }
    public void runNcCommand(String ip, int port, int pingTimeout){
        Process process = null;
        BufferedReader in = null;
        Runtime runtime = Runtime.getRuntime();
        List<String> cmdList = new ArrayList<>();

        cmdList.add("/bin/sh");
        cmdList.add("-c");
        cmdList.add("nc -z -v "+ip+" "+String.valueOf(port));

        String [] array = cmdList.toArray(new String[cmdList.size()]);
        try {
            process = runtime.exec(array);
            sleep(pingTimeout);
            in = new BufferedReader(new InputStreamReader(process.getErrorStream(), "EUC-KR"));
            String msg;
            while ((msg = in.readLine()) != null) {
                result = msg;//in.; //+ System.getProperty("line.separator");
            }
            process.waitFor();
        }
        catch (IOException | NullPointerException e) { System.out.println(e); }
        catch (InterruptedException e) { e.printStackTrace(); }
        finally {
            try {
                if (process !=null) { process.destroy(); }
                if (in !=null ){ in.close(); }
            } catch (IOException ignored) {}
        }
        if(result.contains("refused")){
            isOnline = 0;
            responseTime=0;
        }
        else{
            //Ncat: 0 bytes sent, 0 bytes received in 0.01 seconds.
            isOnline = 1;
            responseTime = (int) (Float.parseFloat(result.split(" ")[8])*1000);
        }
    }
}
